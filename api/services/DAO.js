var mysql = require('mysql');
var connection = null;
//var connect = function () {
//return mysql.createConnection({
//    host: 'localhost',
//    user: 'root',
//    //password: '< MySQL passweword >',
//    database: 'todo'
//});
//};
var pool = (function () {
    return mysql.createPool({
        host: 'localhost',
        user: 'root',
        database: 'todo',
        connectionLimit: 20
    });
})();
var Q = require('q');
var sql = {
    select: 'SELECT * from todolist',
    insert: 'insert into todolist SET ?',
    remove: 'delete from todolist where ?',
    modify: 'UPDATE todolist SET ? WHERE id=?'
};

function getConnection() {
    var deferred = Q.defer();
    pool.getConnection(function (err, conn) {
        if (err)
            deferred.reject(err);
        deferred.resolve(conn);
    });
    return deferred.promise;
};


var exeQuery = function (sql, param) {
    var deffered = Q.defer();
    pool.query(sql, param, function (err, res) {
        if (!err)
            deffered.resolve(res);
        else
            deffered.reject(err);
    });
    return deffered.promise;
};


function routine(nextState) {
    var deferred = Q.defer();

    //진입부
    getConnection().then(function (res) {
        connection = res;
        beginTransaction();
    }, function (err) {
        deferred.reject(err);
    });

    function beginTransaction() {
        connection.beginTransaction(function (err) {
            if (err)
                deferred.reject(err);
            else
                run(nextState('begin', null));
        });
    }

    //실행부
    function run(cur) {
        //기저 사례
        if (cur.name === 'end') {
            connection.commit(function (err) {
                if (err) {
                    deferred.reject(err);
                } else
                    deferred.resolve(cur.ret || {});
            });
            return;
        }
        //재귀호출부
        connection.query(cur.sql, cur.param, function (err, res) {
            if (err)
                deferred.reject(err);
            else
                run(nextState(cur.name, res, cur.param));
        });
    }

    return deferred.promise;
};


function insertRoutine(param) {
    var refresh = false;
    return function (curState, res) {
        var state = {
            begin: function (res) {
                return {name: 'countPost', sql: "select count(*) from todolist"};
            },
            countPost: function (res) {
                refresh = res[0]['count(*)'] > 2;
                if (res[0]['count(*)'] > 2)
                    return {name: 'deletePost', sql: "delete from todolist where true"};
                else
                    return {name: 'insertPost', sql: sql.insert, param: param};
            },
            insertPost: function (res) {
                return {name: 'end', ret: {refresh: refresh, row: param}}
            },
            deletePost: function (res) {
                return {name: 'insertPost', sql: sql.insert, param: param};
            }
        }
        return state[curState](res);
    };
}


module.exports = {

    getTodoList: function () {
        return exeQuery(sql.select);
    },
    insertTodo: function (row) {
        return routine(insertRoutine(row));
    },
    removeTodo: function (id) {
        return exeQuery(sql.remove, {id: id});
    },
    modifyTodo: function (row) {
        return exeQuery(sql.modify, [row, row.id]);
    }
};

