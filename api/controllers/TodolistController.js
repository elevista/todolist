/**
 * TodolistController
 *
 * @description :: Server-side logic for managing todolists
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    get: function (req, res) {
        DAO.getTodoList().then(function (data) {
            res.send(data);
        }, function (err) {
            console.log(err);
            res.status(500).view('500');
        });
    },
    insert: function (req, res) {
        DAO.insertTodo(req.allParams()).then(function (data) {
            res.send(data);
        }, function (err) {
            console.log(err);
            res.status(500).view('500');
        });
    },
    remove: function (req, res) {
        DAO.removeTodo(req.param('id')).then(function (data) {
            res.send(data);
        }, function (err) {
            console.log(err);
            res.status(500).view('500');
        });
    },
    modify: function (req, res) {
        DAO.modifyTodo(req.allParams()).then(function (data) {
            res.send(data);
        }, function (err) {
            console.log(err);
            res.status(500).view('500');
        });
    }
};

