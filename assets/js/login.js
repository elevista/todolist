var todoApp = angular.module('todoApp', ['ui.bootstrap']);

todoApp.controller('todoCtrl', function ($http, $scope, datepickerPopupConfig, datepickerConfig) {
    /* 데이터 바인딩 */
    $scope.items = [];
    $http.get('todolist').success(function (data) {
      $scope.items = data;
    });


    /* Date picker 셋팅 */
    $scope.minDate = new Date();
    $scope.open = function ($event) {
      $scope.status.opened = true;
    };
    $scope.dateFormat = 'yyyy년 M월 d일';
    $scope.status = {
      opened: false
    };
    datepickerConfig.formatDayTitle = "yyyy년 M월";
    datepickerPopupConfig.currentText = "오늘";
    datepickerPopupConfig.closeText = "완료";
    datepickerPopupConfig.clearText = "지우기";


    /* 컨트롤 변수 */
    $scope.editItem = {}; //현재 수정중인 아이템 데이터

    /* 비우기 */
    $scope.clear = function () {
      $scope.editItem = {};
      $scope.editMode = false;
      $scope.editItem.date = Date.now();
    }
    $scope.clear();

    /* 등록 */
    $scope.submit = function () {
      $scope.editItem.date = Number($scope.editItem.date);
      if (!$scope.editMode) { //추가 모드
        $http.post('todolist', $scope.editItem).success(function (data) {
          $scope.items.push(data);
          $scope.clear();
          $scope.minDate = new Date();
        });
      } else {  //수정 모드
        $http.put('todolist/' + $scope.editItem.id, $scope.editItem).success(function (data) {
          $scope.clear();
          $scope.minDate = new Date();
        });
      }
    };

    /* 수정 */
    $scope.edit = function (item) {
      $scope.editMode = true;
      $scope.editItem = item;
      $scope.minDate = 0;
    };

    /* 지우기 */
    $scope.remove = function (item) {
      $http.delete('todolist/' + item.id).success(function () {
        $scope.items.splice($scope.items.indexOf(item), 1);
        if ($scope.editMode)
          $scope.clear();
      });

    }

  }
);

