var todoMain = angular.module('todoMain', ['todoModule']);
todoMain.config(function (todoConfig) {
    todoConfig.clearText = "지우기";
    todoConfig.formatDayTitle = "yyyy년 M월";
    todoConfig.currentText = "오늘";
    todoConfig.closeText = "완료";
}).controller('mainCtrl', function ($scope) {
});

