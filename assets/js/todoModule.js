var todoModule = angular.module('todoModule', ['ui.bootstrap']);
todoModule.constant('todoConfig', {})
    .run(function (todoConfig, datepickerConfig, datepickerPopupConfig) {
        datepickerPopupConfig.clearText = todoConfig.clearText || "지우기";
        datepickerConfig.formatDayTitle = todoConfig.formatDayTitle || "yyyy년 M월";
        datepickerPopupConfig.currentText = todoConfig.currentText || "오늘";
        datepickerPopupConfig.closeText = todoConfig.closeText || "완료";
    })
    .controller('todoController', function ($http, $scope) {
        $scope.abc = 10;
        /* 데이터 바인딩 */
        $scope.items = [];
        $http.get('todolist').success(function (data) {
            $scope.items = data;
        });

        /* Date picker 셋팅 */
        $scope.minDate = new Date();
        $scope.open = function ($event) {
            $scope.status.opened = true;
        };
        $scope.dateFormat = 'yyyy년 M월 d일';
        $scope.status = {
            opened: false
        };

        /* 컨트롤 변수 */
        $scope.editItem = {}; //현재 수정중인 아이템 데이터

        /* 비우기 */
        var clear = function () {
            $scope.editItem = {};
            $scope.editMode = false;
            $scope.editItem.date = Date.now();
        }
        clear();

        /* 등록 */
        $scope.submit = function () {
            $scope.editItem.date = Number($scope.editItem.date);
            if (!$scope.editMode) { //추가 모드
                $http.post('todolist', $scope.editItem).success(function (data) {
                    clear();
                    $scope.minDate = new Date();
                    if (data.refresh)
                        $http.get('todolist').success(function (data) {
                            $scope.items = data;
                        });
                    else
                        $scope.items.push(data.row);
                });
            } else {  //수정 모드
                $http.put('todolist/' + $scope.editItem.id, $scope.editItem).success(function (data) {
                    clear();
                    $scope.minDate = new Date();
                });
            }
        };

        /* 수정 */
        $scope.edit = function (item) {
            $scope.editMode = true;
            $scope.editItem = item;
            $scope.minDate = 0;
        };

        /* 지우기 */
        $scope.remove = function (item) {
            $http.delete('todolist/' + item.id).success(function () {
                $scope.items.splice($scope.items.indexOf(item), 1);
                if ($scope.editMode)
                    clear();
            });

        }

    }).directive('todoDirective', function () {
    return {
        scope: {},
        templateUrl: "templates/todoTemplate.html",
        controller: 'todoController',
        controllerAs: 'ctrl'
    };
});

